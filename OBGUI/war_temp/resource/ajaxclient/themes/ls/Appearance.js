/**
 * The simple qooxdoo appearance theme.
 *
 * @asset(qx/icon/Tango/16/apps/office-calendar.png)
 * @asset(qx/icon/Tango/16/places/folder-open.png)
 * @asset(qx/icon/Tango/16/places/folder.png)
 * @asset(qx/icon/Tango/16/mimetypes/text-plain.png)
 * @asset(qx/icon/Tango/16/actions/view-refresh.png)
 * @asset(qx/icon/Tango/16/actions/window-close.png)
 * @asset(qx/icon/Tango/16/actions/dialog-cancel.png)
 * @asset(qx/icon/Tango/16/actions/dialog-ok.png)
 */
qx.Theme.define("qx.theme.ls.Appearance", {
    extend: ajaxclient.theme.simple.Appearance,

    appearances: {
        /* AJAX TEXT */
        //textfield
        "customtext": {
            alias: "textfield",
            include: "textfield",
            style: function(states) {

                return {
                    decorator: states.focused ? "textfield-focused" : "textfield",
                    textColor: states.focused ? "#FFD0DA" : "#FF0000"
                };
            }
        },

        /* AJAX TEXTAREA */
        //textfield
        "customtextarea": {
            alias: "textfield",
            include: "textfield",
            style: function(states) {

                return {
                    decorator: !states.focused ? "textfield-focused" : "textfield",
                    textColor: states.focused ? "#FBABDA" : "#FFABBA"
                };
            }
        },

        /* AJAX PASSWORD */
        //textfield
        "custompassword": {
            style: function(states) {

                return {
                    decorator: states.focused ? "custompassword-focused" : "custompassword",
                    textColor: states.focused ? "blue" : "red"
                };
            }
        },

        // widget
        "customimage": {
            style: function(states) {

                return {
                    padding: states.focused ? 3 : 35,
                    decorator: "customimage"
                };
            }
        },

        //spinner
        "customspinner": {
            include: "spinner",
            style: function(states) {

                return {
                    decorator: "customspinner",
                    textColor: states.focused ? "red" : "blue",
                    padding: 10
                };
            }
        },

        "customspinner/textfield": {
            style: function(states) {
                return {
                    decorator: states.focused ? "spinnertextfield-focused" : "spinnertextfield",
                    padding: 10
                };
            }
        },
        "customspinner/upbutton": "spinner/upbutton",
        "customspinner/downbutton": "spinner/downbutton",

        //scrollarea
        "customcontainer": {
            style: function() {
                return {
                    decorator: "customcontainer",
                    padding: 5
                };
            }
        },

        //tree
        "customtree": {
            style: function() {
                return {
                    decorator: "customtree",
                    padding: 5
                };
            }
        },
        //groupbox
        "customgroup": {
            style: function() {
                return {
                    decorator: "customgroupbox",
                    padding: 5
                };
            }
        },

        "customgroup/frame": {
            style: function() {
                return {
                    decorator: "groupframe",
                    padding: 5
                };
            }
        },

        //tabview
        "customtabview": {
            style: function() {
                return {
                    decorator: "groupframe",
                    padding: 5
                };
            }
        },

        "customtabview/pane": {
            style: function() {
                return {
                    decorator: "tabviewpane",
                    padding: 5
                };
            }
        },

        "customtabview/bar/scrollpane": {
            style: function() {
                return {};
            }
        },

        "customtabview/bar/content": {
            style: function() {
                return {};
            }
        },

        //combobox
        "customcombobox": {
            style: function() {
                return {
                    padding: 10,
                    decorator: "customcombobox"
                };
            }
        },

        "customcombobox/button/icon": "combobox/button/icon",
        "customcombobox/textfield": {
            style: function(states) {
                return {
                    textColor: states.focused ? "red" : "yellow"
                };
            }
        },
        "customcombobox/list/pane": "widget",
        "customcombobox/popup": "widget",
        "customcombobox/list": {
            style: function() {
                return {
                    decorator: "comboboxpane"
                };
            }
        },

        "customcombobox/button": {
            include: "combobox/button",
            style: function(states) {
                var decorator;
                if(states.pressed) {
                    decorator = "combobutton-pressed";
                } else if(states.hovered) {
                    decorator = "combobutton-hovered";
                } else {
                    decorator = "combobutton";
                }
                return {
                    padding: 10,
                    margin: 2,
                    decorator: decorator
                };
            }
        },

        //datefield
        "customdatepicker": {
            style: function() {
                return {
                    padding: 10,
                    decorator: "customdatepicker"
                };
            }
        },
        "customdatepicker/button": "datefield/button",
        "customdatepicker/list/day": "datefield/list/day",
        "customdatepicker/list/month-year-label": "datefield/list/month-year-label",
        "customdatepicker/list/last-month-button": "datefield/list/last-month-button",
        "customdatepicker/list/last-year-button": "datefield/list/last-year-button",
        "customdatepicker/list/navigation-bar": "datefield/list/navigation-bar",
        "customdatepicker/list/week": "datefield/list/week",
        "customdatepicker/list/weekday": "datefield/list/weekday",
        "customdatepicker/list/date-pane": {
            include: "datefield/list/date-pane",
            style: function() {
                return {
                    textColor: "blue"
                };
            }
        },
        "customdatepicker/list/next-month-button": "datefield/list/next-month-button",
        "customdatepicker/list/next-year-button": "datefield/list/next-year-button",
        "customdatepicker/textfield": {
            style: function(states) {
                return {
                    padding: 10,
                    textColor: states.focused ? "#002EB8" : "#33CCFF",
                    decorator: !states.focused ? "customdatepicker" : "customdatepickertext"
                };
            }
        },

        "customcheckbox/icon": "checkbox/icon",

        //checkbox
        "customcheckbox": {
            include: "checkbox",
            style: function(states) {
                return {
                    textColor: states.checked ? "blue" : states.focused ? "green" : "red"
                };
            }
        },

        //progressbar
        "customprogressbar": {
            style: function() {
                return {
                    padding: 2,
                    decorator: "customprogressbar"
                };
            }
        },
        "customprogressbar/progress": {
            style: function() {
                return {
                    backgroundColor: "#33FF66"
                };
            }
        },

        //button
        "custombutton": {
            style: function(states) {
                var decorator,
                    textColor = "#D5D5D5";

                if(states.pressed) {
                    decorator = "custombutton-pressed";
                } else if(states.hovered) {
                    decorator = "custombutton-hovered";
                } else if(states.disabled) {
                    decorator = "custombutton-pressed";
                    textColor = "#DD00D0";
                } else {
                    decorator = "custombutton";
                }

                return {
                    margin: 2,
                    textColor: textColor,
                    decorator: decorator
                };
            }
        },

        //label
        "customlabel": {
            style: function(states) {
                return {
                    textColor: states.focused ? "#002EB8" : "#33CCFF"
                };
            }
        },

        //toolbar
        "customtoolbar": {
            style: function() {
                return {
                    padding: 5,
                    decorator: "customtoolbar"
                };
            }
        },

        //toolbar-button
        "customtoolbarbutton": "custombutton",

        //toolbar-button
        "customtoolbarcheckbox": "custombutton",

        //toolbar-button
        "customtoolbarradio": "custombutton",

        //toolbar-menubutton
        "customtoolbarmenu": {
            style: function() {
                return {
                    decorator: "customtoolbarmenu"
                };
            }
        },

        "customsplitbutton": {
            style: function() {
                return {
                    decorator: "customsplitbutton"
                };
            }
        },

        //toolbar-separator
        "customtoolbarseparator": {
            style: function() {
                return {
                    decorator: "customtoolbarseparator",
                    margin: [17, 10],
                    width: 14
                };
            }
        },

        "customsplitbutton/button": "custombutton",
        "customsplitbutton/arrow": "splitbutton/arrow",
        "customsplitbutton/arrow/icon": "splitbutton/arrow/icon",

        //container
        "container": {
            style: function() {
                return {
                    decorator: "defaultscrollarea"
                };
            }
        },

        "container/corner": "scrollarea/corner",

        "container/pane": "scrollarea/pane",
        "container/scrollbar-x": "scrollarea/scrollbar-x",
        "container/scrollbar-y": "scrollarea/scrollbar-y",
        "container/scrollbar-x/button-begin": "scrollarea/scrollbar-x/button-begin",
        "container/scrollbar-x/button-end": "scrollarea/scrollbar-x/button-end",
        "container/scrollbar-y/button-begin": "scrollarea/scrollbar-y/button-begin",
        "container/scrollbar-y/button-end": "scrollarea/scrollbar-y/button-end",

        //image
        "imagecomponent": {
            style: function() {
                return {
                    decorator: undefined
                };
            }
        },

        /*
         ---------------------------------------------------------------------------
         PAGE
         ---------------------------------------------------------------------------
         */

        "page": {
            include: "scrollarea"
        },

        "page/corner": "scrollarea/corner",

        "page/pane": "scrollarea/pane",
        "page/scrollbar-x": "scrollarea/scrollbar-x",
        "page/scrollbar-y": "scrollarea/scrollbar-y",
        "page/scrollbar-x/button-begin": "scrollarea/scrollbar-x/button-begin",
        "page/scrollbar-x/button-end": "scrollarea/scrollbar-x/button-end",
        "page/scrollbar-y/button-begin": "scrollarea/scrollbar-y/button-begin",
        "page/scrollbar-y/button-end": "scrollarea/scrollbar-y/button-end",

        /*
         ---------------------------------------------------------------------------
         PIE CHART
         ---------------------------------------------------------------------------
         */

        "piechart": {
            style: function() {
                return {
                    decorator: undefined
                };
            }
        },

        /*
         ---------------------------------------------------------------------------
         COLUMN CHART
         ---------------------------------------------------------------------------
         */

        "columnchart": {
            style: function() {
                return {
                    decorator: undefined
                };
            }
        }
    }
});
